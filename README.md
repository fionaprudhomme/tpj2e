# TPJ2E

## Objectifs

Ecrire une petite application de gestion des inscriptions de participants à des évenements. 
Nous utiliserons le Framework Spring pour l’architecture MVC, Hibernate pour la couche depersitance, Maven pour la gestion des dépendances.

## Lancer l'application

1. Dans l'invit de commande (dans le dossier demo):
    1. sudo -i -u postgres
    2. psql
    2. CREATE USER formation;
    3. ALTER ROLE formation WITH CREATEDB;
    4. CREATE DATABASE formation OWNER formation;
    5. ALTER USER formation WITH ENCRYPTED PASSWORD 'formation20192020';
    6. ./mvnw spring-boot:run

   

## Application

- Aller sur localhost:8080/menu

- Ajouter un évenement

- Retourner sur localhost:8080/menu

- Ajouter un ou des participants

  