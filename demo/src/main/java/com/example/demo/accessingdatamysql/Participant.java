package com.example.demo.accessingdatamysql;

import java.sql.Array;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.istack.NotNull;


@Entity
@Table(name= "Participant")
public class Participant {
	
	// BDD
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	

	@Column(name = "num_pers")
	private int numpers;
	
	@NotNull
	@Column(name = "nom", nullable = false)
	private String nom;
	
	@NotNull
	@Column(name = "prenom", nullable = false)
	private String prenom;
	
	@NotNull
	@Column(name = "email", nullable = false)
	private String email;
	
	@Column(name = "dateNaiss", nullable = false)
	private String datenaiss;
	
	@Column(name= "eventName", nullable = false)
	private String eventName;
	
	// lien aux evenements (1 evenement pour plusieurs participants)
	@ManyToOne
	private Evenement event;
	
	// Getter et Setter 

	public int getNumpers() {
		return numpers;
	}
	
	public void setNumpers(int numpers) {
		this.numpers = numpers;
	}
	public String getDatenaiss() {
		return datenaiss;
	}
	public void setDatenaiss(String datenaiss) {
		this.datenaiss = datenaiss;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
		
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
		
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
		
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEvent(Evenement event) {
		this.event = event;
	}
	public Evenement getEvent() {
		return event;
	}

}
