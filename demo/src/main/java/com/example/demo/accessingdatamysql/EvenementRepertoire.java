package com.example.demo.accessingdatamysql;

import org.springframework.data.repository.CrudRepository;

public interface EvenementRepertoire  extends CrudRepository<Evenement, Integer>{

}
