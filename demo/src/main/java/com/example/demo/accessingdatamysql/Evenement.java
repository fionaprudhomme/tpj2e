package com.example.demo.accessingdatamysql;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sun.istack.NotNull;

@Entity
@Table(name= "Event")
public class Evenement {
	// BDD
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")

	@Column(name = "num_event")
	private int numevent;
	
	@NotNull
    @Column(name = "intitule", nullable = false)
    private String intitule;
	
	@NotNull
    @Column(name = "theme", nullable = false)
    private String theme;
	
	@NotNull
    @Column(name = "datedebut", nullable = false)
    private String datedebut;
	
	@NotNull
    @Column(name = "duree", nullable = false)
    private int duree;

	// lien aux participants (1 evenement pour plusieurs participants)
    @OneToMany(mappedBy="event")
    private Collection<Participant> participants ;
    
    
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getDatedebut() {
		return datedebut;
	}
	public void setDatedebut(String datedebut) {
		this.datedebut = datedebut;
	}
	public int getDuree() {
		return duree;
	}
	public void setDuree(int duree) {
		this.duree = duree;
	}
	public int getID() {
		return numevent;
	}
	public void setID(int numevent) {
		this.numevent = numevent;
	}
  
    
	


}
