package com.example.demo.accessingdatamysql;

import org.hibernate.SessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		//Démarrage de l'application
		SpringApplication.run(DemoApplication.class, args);
		
	}

}
